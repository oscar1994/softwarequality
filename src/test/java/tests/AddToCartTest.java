package tests;

import org.testng.Assert;
import org.testng.annotations.Test;


import pages.LaptopListPage;
import pages.UserLoginPage;

public class AddToCartTest extends BaseTest {
	
	 @Test(priority = 1)
	    public void ListLaptops(){
	        LaptopListPage ListPage = new LaptopListPage(driver);
	        ListPage.openLaptopsPage();
	        
	    }
	 
	 @Test(priority = 2)
	 public void AddToCartWithExternalButton() {
		 LaptopListPage ListPage = new LaptopListPage(driver);
	     ListPage.openLaptopsPage();
	     ListPage.showHiddenCartButton(10);
	     ListPage.clickExternalButtonCart();
	     Assert.assertEquals(ListPage.getTextSuccessFromCart(10), "PRODUCTO AÑADIDO CORRECTAMENTE A SU CARRITO DE LA COMPRA");
	     
	 }

	 
	 @Test(priority = 3)
	 public void AddToCartWithInternalButton() {
		 LaptopListPage ListPage = new LaptopListPage(driver);
	     ListPage.openLaptopsPage();
	     ListPage.clickInLaptop();
	     ListPage.clickInternalButtonCart();
	     Assert.assertEquals(ListPage.getTextSuccessFromCart(10), "PRODUCTO AÑADIDO CORRECTAMENTE A SU CARRITO DE LA COMPRA");
	     
	 }

}
