package tests;

import org.testng.annotations.Test;
import pages.HomePage;

public class HomeTest extends BaseTest {

    @Test
    public void openHomePage () {
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
    }

}
