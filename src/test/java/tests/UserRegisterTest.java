package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.UserRegisterPage;

public class UserRegisterTest extends BaseTest {

    @Test
    public void openMenuForLogin() {
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserRegisterPage userRegisterPage = new UserRegisterPage(driver);
        userRegisterPage.openMenuLogin();
    }

    @Test
    public void clickOptionLogin() {
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserRegisterPage userRegisterPage = new UserRegisterPage(driver);
        userRegisterPage.openMenuLogin();
        userRegisterPage.clickOptionLogin();
    }

    @Test
    public void registerWithOutEmail() {
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserRegisterPage userRegisterPage = new UserRegisterPage(driver);
        userRegisterPage.openMenuLogin();
        userRegisterPage.clickOptionLogin();
        userRegisterPage.clickButtonToCreateAccount();
        Assert.assertEquals(userRegisterPage.getTextFromErrorEmail(2),"Dirección email inválida.");
    }

    @Test
    public void registerEmailWithInvalidFormat(){
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserRegisterPage userRegisterPage = new UserRegisterPage(driver);
        userRegisterPage.openMenuLogin();
        userRegisterPage.clickOptionLogin();
        userRegisterPage.clickEmailInputAndWrite("email@invalidFormat");
        userRegisterPage.clickButtonToCreateAccount();
        Assert.assertEquals(userRegisterPage.getTextFromErrorEmail(2),"Dirección email inválida.");
    }

    @Test
    public void registerWithOutValues() throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserRegisterPage userRegisterPage = new UserRegisterPage(driver);
        userRegisterPage.openMenuLogin();
        userRegisterPage.clickOptionLogin();
        userRegisterPage.clickEmailInputAndWrite("orvega@est.utn.ac.cr");
        userRegisterPage.clickButtonToCreateAccount();
        Thread.sleep(5000);
        userRegisterPage.clearEmailInput();
        userRegisterPage.clickButtonSignIn(10);
        Assert.assertEquals(userRegisterPage.getTextFromForm(10),"Hay 4 errores");
    }

    @Test
    public void registerWithValuesAndIncorrectBirthDate() {
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserRegisterPage userRegisterPage = new UserRegisterPage(driver);
        userRegisterPage.openMenuLogin();
        userRegisterPage.clickOptionLogin();
        userRegisterPage.clickEmailInputAndWrite("sofia@qwert.com");
        userRegisterPage.clickButtonToCreateAccount();
        userRegisterPage.clickInputAndWrite("Sofia", "Rojas", "123456", "30", "12", "2019", 5);
        userRegisterPage.clickButtonSignIn(10);
        Assert.assertEquals(userRegisterPage.getTextFromForm(10),"Hay un 1 error");
    }

}
