package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.UserLoginPage;

public class UserLoginTest extends BaseTest {
	
    @Test(priority = 1)
    public void signInWithOutCredentials(){
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserLoginPage userLoginPage = new UserLoginPage(driver);
        userLoginPage.openMenuLogin();
        userLoginPage.clickOptionLogin();
        userLoginPage.clickSignInButton();
        Assert.assertEquals(userLoginPage.getTextFromErrorCredentials(10), "Hay un 1 error");
    }

    @Test(priority = 2)
    public void sigInWithInvalidCredentials(){
        HomePage homePage = new HomePage(driver);
        homePage.openHomePage();
        UserLoginPage userLoginPage = new UserLoginPage(driver);
        userLoginPage.openMenuLogin();
        userLoginPage.clickOptionLogin();
        userLoginPage.fillInputWithCredentials("qweqwe@qwe.com", "123456780", 8);
        userLoginPage.clickSignInButton();
        Assert.assertEquals(userLoginPage.getTextFromErrorCredentials(10), "Hay un 1 error");
    }
    
	
	  @Test(priority = 3) 
	  public void sigInWithCorrectCredentials(){ HomePage
	  homePage = new HomePage(driver); homePage.openHomePage(); UserLoginPage
	  userLoginPage = new UserLoginPage(driver); userLoginPage.openMenuLogin();
	  userLoginPage.clickOptionLogin();
	  userLoginPage.fillInputWithCredentials("lvilllaobos@gmail.com", "hola123", 8); 
	  userLoginPage.clickSignInButton();
	  Assert.assertEquals(userLoginPage.getTextFromWelcomeMessage(10), "MI CUENTA"); 
	  }
	 
    
}
