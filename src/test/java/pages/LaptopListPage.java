package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LaptopListPage extends BasePage {

	private static final String LAPTOP_LIST_URL = "http://extremetechcr.com/tienda/39-laptop";
	private WebDriver webDriver;
	private WebDriverWait webDriverWait;
	private WebElement LaptopItem;
	private WebElement ExternalButton;
	private WebElement InternalButton;
	private WebElement CredentialsSuccess;
	
	
	
	
	public LaptopListPage(WebDriver webDriver){
        this.webDriver = webDriver;
        
        
    }
	
	public void openLaptopsPage(){
        this.webDriver.get(LAPTOP_LIST_URL);
        this.LaptopItem = this.webDriver.findElement(By.xpath("//*[@id=\"center_column\"]/div[3]/div[1]/div/div[1]/div"));
    }
	
	public void showHiddenCartButton(int timeOutSeconds) {
		
		
		Actions builder = new Actions(webDriver);
        builder.moveToElement(this.LaptopItem).build().perform();
      
        
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);
        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"center_column\"]/div[3]/div[1]/div/div[2]/div/div[3]")));
        this.ExternalButton = webDriver.findElement(By.xpath("//*[@id=\"center_column\"]/div[3]/div[1]/div/div[2]/div/div[3]"));
        
	}
	
	public void clickExternalButtonCart() {
		
		click(this.ExternalButton);
		
	}
	
	public void clickInternalButtonCart() {
		this.InternalButton = this.webDriver.findElement(By.xpath("//*[@id=\"add_to_cart\"]/button"));
		click(this.InternalButton);
		
	}
	
	public void clickInLaptop() {
		click(LaptopItem);
	}
	
	
	public String getTextSuccessFromCart(int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);
        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[1]/span[2]")));
        this.CredentialsSuccess = webDriver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[1]/span[2]"));
        return readText(this.CredentialsSuccess);
    }
	
	

	
	
	
}
