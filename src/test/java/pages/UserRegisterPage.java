package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserRegisterPage extends BasePage {

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private WebElement dropdownUserMenu;
    private WebElement loginOption;
    private WebElement emailCreateInput;
    private WebElement createAccountButton;
    private WebElement emailError;
    private WebElement emailInput;
    private WebElement signInButton;
    private WebElement formError;
    private WebElement name;
    private WebElement lastName;
    private WebElement password;
    private WebElement userTitle;


    public UserRegisterPage(WebDriver webDriver){
        this.webDriver = webDriver;
        this.dropdownUserMenu = this.webDriver.findElement(By.xpath("//*[@id=\"topbar\"]/div/div/div/div[1]/div[2]"));
        this.loginOption = this.webDriver.findElement(By.xpath("//*[@id=\"topbar\"]/div/div/div/div[1]/div[2]/div[2]/ul/li[1]/a"));
    }

    public void openMenuLogin(){
        Actions builder = new Actions(webDriver);
        Action hoverAndClick = builder.moveToElement(this.dropdownUserMenu).click(this.dropdownUserMenu).build();
        hoverAndClick.perform();
    }

    public void clickOptionLogin(){
        click(this.loginOption);
    }

    public void clickButtonToCreateAccount(){
        this.createAccountButton = webDriver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]"));
        click(this.createAccountButton);
    }

    public String getTextFromErrorEmail(int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);
        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"create_account_error\"]/ol/li")));
        this.emailError = webDriver.findElement(By.xpath("//*[@id=\"create_account_error\"]/ol/li"));
        return readText(this.emailError);
    }

    public void clickEmailInputAndWrite(String email){
        this.emailCreateInput = this.webDriver.findElement(By.xpath("//*[@id=\"email_create\"]"));
        click(this.emailCreateInput);
        writeText(this.emailCreateInput,email);
    }

    public void clearEmailInput(){
        this.emailInput = this.webDriver.findElement(By.xpath("//*[@id=\"email\"]"));
        this.emailInput.clear();
    }

    public void clickButtonSignIn(int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);
        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"submitAccount\"]")));
        this.signInButton = this.webDriver.findElement(By.xpath("//*[@id=\"submitAccount\"]"));
        click(this.signInButton);
    }

    public String getTextFromForm(int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);
        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"center_column\"]/div/p")));
        this.formError = webDriver.findElement(By.xpath("//*[@id=\"center_column\"]/div/p"));
        return readText(this.formError);
    }

    public void clickInputAndWrite(String name, String lastName, String password, String day, String month, String year, int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"id_gender1\"]")));
        this.userTitle = this.webDriver.findElement(By.xpath("//*[@id=\"id_gender1\"]"));
        click(this.userTitle);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"customer_firstname\"]")));
        this.name = this.webDriver.findElement(By.xpath("//*[@id=\"customer_firstname\"]"));
        click(this.name);
        writeText(this.name,name);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"customer_lastname\"]")));
        this.lastName = this.webDriver.findElement(By.xpath("//*[@id=\"customer_lastname\"]"));
        click(this.lastName);
        writeText(this.lastName,lastName);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"passwd\"]")));
        this.password = this.webDriver.findElement(By.xpath("//*[@id=\"passwd\"]"));
        click(this.password);
        writeText(this.password,password);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"days\"]")));
        Select birthDay = new Select(this.webDriver.findElement(By.xpath("//*[@id=\"days\"]")));
        birthDay.selectByValue(day);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"months\"]")));
        Select birthMonth = new Select(this.webDriver.findElement(By.xpath("//*[@id=\"months\"]")));
        birthMonth.selectByValue(month);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"years\"]")));
        Select birthYear = new Select(this.webDriver.findElement(By.xpath("//*[@id=\"years\"]")));
        birthYear.selectByValue(year);
    }

}
