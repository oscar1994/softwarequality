package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserLoginPage extends BasePage {

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private WebElement dropdownUserMenu;

    private WebElement loginOption;
    private WebElement signInButton;

    private WebElement credentialsError;
    private WebElement credentialsSucces;
    private WebElement email;
    private WebElement password;

    public UserLoginPage(WebDriver webDriver){
        this.webDriver = webDriver;
        this.dropdownUserMenu = this.webDriver.findElement(By.xpath("//*[@id=\"topbar\"]/div/div/div/div[1]/div[2]"));
        this.loginOption = this.webDriver.findElement(By.xpath("//*[@id=\"topbar\"]/div/div/div/div[1]/div[2]/div[2]/ul/li[1]/a"));
       
        
    }

    public void openMenuLogin(){
        Actions builder = new Actions(webDriver);
        Action hoverAndClick = builder.moveToElement(this.dropdownUserMenu).click(this.dropdownUserMenu).build();
        hoverAndClick.perform();
    }
    
    
  
    

    public void clickOptionLogin(){
        click(this.loginOption);
    }
    
   


    public void clickSignInButton(){
        this.signInButton = this.webDriver.findElement(By.xpath("//*[@id=\"SubmitLogin\"]"));
        click(this.signInButton);
    }
    

    public String getTextFromErrorCredentials(int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);
        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"center_column\"]/div[1]/p[1]")));
        this.credentialsError = webDriver.findElement(By.xpath("//*[@id=\"center_column\"]/div[1]/p[1]"));
        return readText(this.credentialsError);
    }
    
    public String getTextFromWelcomeMessage(int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);
        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"center_column\"]/h1")));
        this.credentialsSucces = webDriver.findElement(By.xpath("//*[@id=\"center_column\"]/h1"));
        return readText(this.credentialsSucces);
    }

    public void fillInputWithCredentials(String email, String password, int timeOutSeconds){
        this.webDriverWait = new WebDriverWait(webDriver,timeOutSeconds);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"email\"]")));
        this.email = this.webDriver.findElement(By.xpath("//*[@id=\"email\"]"));
        click(this.email);
        writeText(this.email,email);

        this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"passwd\"]")));
        this.password = this.webDriver.findElement(By.xpath("//*[@id=\"passwd\"]"));
        click(this.password);
        writeText(this.password, password);
    }

}
