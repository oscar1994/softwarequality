package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    private WebDriverWait wait;

    public void click (WebElement webElement) {
        webElement.click();
    }

    public void writeText (WebElement webElement, String text) {
        webElement.sendKeys(text);
    }

    public String readText (WebElement webElement) {
        return webElement.getText();
    }

}
