package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class HomePage extends BasePage {

    private static final String HOME_TEST_URL = "http://extremetechcr.com/tienda/";
    private WebDriver webDriver;

    public HomePage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void openHomePage(){
        this.webDriver.get(HOME_TEST_URL);
    }

}
